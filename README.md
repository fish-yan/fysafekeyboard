# FYSafeKeyboard

[![CI Status](https://img.shields.io/travis/757094197@qq.com/FYSafeKeyboard.svg?style=flat)](https://travis-ci.org/757094197@qq.com/FYSafeKeyboard)
[![Version](https://img.shields.io/cocoapods/v/FYSafeKeyboard.svg?style=flat)](https://cocoapods.org/pods/FYSafeKeyboard)
[![License](https://img.shields.io/cocoapods/l/FYSafeKeyboard.svg?style=flat)](https://cocoapods.org/pods/FYSafeKeyboard)
[![Platform](https://img.shields.io/cocoapods/p/FYSafeKeyboard.svg?style=flat)](https://cocoapods.org/pods/FYSafeKeyboard)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FYSafeKeyboard is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FYSafeKeyboard'
```

## Author

757094197@qq.com, 757094197@qq.com

## License

FYSafeKeyboard is available under the MIT license. See the LICENSE file for more info.
