//
//  JODES3Util.swift
//  joint-operation
//
//  Created by Yan on 2018/12/3.
//  Copyright © 2018 Yan. All rights reserved.
//

import UIKit
import CommonCrypto

private let key = "201707194267420170719426"
private let iv = "01234567"

class FYDES3Util: NSObject {
    
    /// 3DES加密
    static func encrypt(_ input: String) -> String? {
        return DES3Coding(input, type: kCCEncrypt)
    }
    
    /// 3DES解密
    static func decrypt(_ input: String) -> String? {
        return DES3Coding(input, type: kCCDecrypt)
    }
    
    private static func DES3Coding(_  input: String, type: Int) -> String? {
        // TODO: 创建要加密或解密的数据接受对象
        var data = Data.init()
        // 加密
        if type == kCCEncrypt {
            // 将要加密的对象编码
            data = input.data(using: .utf8)!
        }
        // 解密
        if type == kCCDecrypt {
            // 将要解密的对象编码
            data = Data.init(base64Encoded: input, options: .ignoreUnknownCharacters)!
        }
        // 创建数据编码后的指针
        let dataPointer = UnsafeRawPointer((data as NSData).bytes)
        // 获取转码后数据的长度
        let dataLength = size_t(data.count)
        
        // TODO: 将加密或解密的密钥转化为Data数据
        let keyData = key.data(using: .utf8)! as NSData
        // 创建密钥的指针
        let keyPointer = UnsafeRawPointer(keyData.bytes)
        // 创建向量指针
        let ivData = iv.data(using: .utf8)! as NSData
        let initVec = UnsafeRawPointer(ivData.bytes)
        // 设置密钥的长度
        let keyLength = size_t(kCCKeySize3DES)
        
        // TODO: 创建加密或解密后的数据对象
        let cryptData = NSMutableData(length: Int(dataLength) + kCCBlockSize3DES)
        // 获取返回数据(cryptData)的指针
        let cryptPointer = UnsafeMutableRawPointer(mutating: cryptData!.mutableBytes)
        // 获取接收数据的长度
        let cryptDataLength = size_t(cryptData!.length)
        // 加密或则解密后的数据长度
        var cryptBytesLength:size_t = 0
        
        // TODO: 数据参数的准备
        // 是解密或者加密操作(CCOperation 是32位的)
        let operation:CCOperation = UInt32(type)
        // 算法的类型
        let algorithm:CCAlgorithm = UInt32(kCCAlgorithm3DES)
        // 设置密码的填充规则（ PKCS7 & ECB 两种填充规则）
        let options:CCOptions = UInt32(kCCOptionPKCS7Padding)
        // 执行算法处理
        let cryptStatue = CCCrypt(operation, algorithm, options, keyPointer, keyLength,initVec, dataPointer, dataLength, cryptPointer, cryptDataLength, &cryptBytesLength)
        // 通过返回状态判断加密或者解密是否成功
        if  UInt32(cryptStatue) == kCCSuccess  {
            // 加密
            if type == kCCEncrypt {
                cryptData!.length = cryptBytesLength
                // 返回3des加密对象
                return cryptData!.base64EncodedString(options: .lineLength64Characters)
            }
            // 解密
            if type == kCCDecrypt {
                // 返回3des解密对象
                cryptData!.length = cryptBytesLength
                let outString = NSString(data:cryptData! as Data ,encoding:String.Encoding.utf8.rawValue)
                return outString! as String
            }
        }
        // 3des 加密或者解密不成功
        return nil
    }

}
