//
//  FYTextField.swift
//  hook
//
//  Created by yan on 2020/9/10.
//  Copyright © 2020 yan. All rights reserved.
//

import UIKit

protocol FYTextFieldDelegate: NSObjectProtocol {
    
    func textFieldDidBeginEditing(_ textField: FYTextField)
    
    func textFieldEditingChanged(_ textField: FYTextField, texts: [String])
    
    func textFieldDidEndEditing(_ textField: FYTextField, text: String)
}
extension FYTextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: FYTextField) {}
    
    func textFieldEditingChanged(_ textField: FYTextField, texts: [String]) {}
    
    func textFieldDidEndEditing(_ textField: FYTextField, text: String) {}
}

class FYTextField: UITextField, UITextFieldDelegate {
    
    weak open var fy_delegate: FYTextFieldDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setInputView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setInputView()
    }
    
    private func setInputView() {
        inputView = FYKeyboard()
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        UIMenuController.shared.isMenuVisible = false
        return false
    }
}
