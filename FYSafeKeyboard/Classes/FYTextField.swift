//
//  FYTextField.swift
//  hook
//
//  Created by yan on 2020/9/10.
//  Copyright © 2020 yan. All rights reserved.
//

import UIKit

public protocol FYTextFieldDelegate: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: FYTextField)
    
    func textFieldEditingChanged(_ textField: FYTextField, texts: [String])
    
    func textFieldDidEndEditing(_ textField: FYTextField, text: String)
    
}
public extension FYTextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: FYTextField) {}
    
    func textFieldEditingChanged(_ textField: FYTextField, texts: [String]) {}
    
    func textFieldDidEndEditing(_ textField: FYTextField, text: String) {}
}

public class FYTextField: UITextField {
    
    weak open var fy_delegate: FYTextFieldDelegate? {
        didSet {
            self.delegate = fy_delegate
        }
    }
    
    open var isRandomNum = false {
        didSet {
            let input = inputView as? FYKeyboard
            input?.isRandomNum = isRandomNum
        }
    }
    
    open var isRandomChar = false {
        didSet {
            let input = inputView as? FYKeyboard
            input?.isRandomChar = isRandomChar
        }
    }
    
    open var safeKeyboardType = FYSafeKeyboardType.numChar
                
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setInputView()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setInputView()
    }
    
    private func setInputView() {
        inputView = FYKeyboard()
        isSecureTextEntry = true
        clearsOnBeginEditing = true
    }

    public override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        UIMenuController.shared.isMenuVisible = false
        return false
    }
}

public enum FYSafeKeyboardType: Int {
    case numChar = 0
    case all = 1
}
