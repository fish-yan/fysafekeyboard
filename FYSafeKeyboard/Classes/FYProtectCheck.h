//
//  FYProtect.h
//  hook
//
//  Created by yan on 2020/9/10.
//  Copyright © 2020 yan. All rights reserved.
//

#ifndef FYProtectCheck_h
#define FYProtectCheck_h

#import <sys/sysctl.h>
#import <sys/stat.h>
#import <dlfcn.h>
#import <Foundation/Foundation.h>

static __attribute__((always_inline)) void asm_ptrace() {
#ifdef __arm64__
    __asm__("mov X0, #31\n"
            "mov X1, #0\n"
            "mov X2, #0\n"
            "mov X3, #0\n"
            "mov X16, #26\n"
            "svc #0x80\n"
            );
#endif
}

void  checkCodesign(NSString *ids){
#ifdef __arm64__
    // 描述文件路径
    NSString *embeddedPath = [[NSBundle mainBundle] pathForResource:@"embedded" ofType:@"mobileprovision"];
    // 读取application-identifier  注意描述文件的编码要使用:NSASCIIStringEncoding
    NSString *embeddedProvisioning = [NSString stringWithContentsOfFile:embeddedPath encoding:NSASCIIStringEncoding error:nil];
    NSArray *embeddedProvisioningLines = [embeddedProvisioning componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    for (int i = 0; i < embeddedProvisioningLines.count; i++) {
        if ([embeddedProvisioningLines[i] rangeOfString:@"application-identifier"].location != NSNotFound) {
            
            NSInteger fromPosition = [embeddedProvisioningLines[i+1] rangeOfString:@"<string>"].location+8;
            
            NSInteger toPosition = [embeddedProvisioningLines[i+1] rangeOfString:@"</string>"].location;
            
            NSRange range;
            range.location = fromPosition;
            range.length = toPosition - fromPosition;
            
            NSString *fullIdentifier = [embeddedProvisioningLines[i+1] substringWithRange:range];
            NSArray *identifierComponents = [fullIdentifier componentsSeparatedByString:@"."];
            NSString *appIdentifier = [identifierComponents firstObject];
            // 对比签名ID
            if (![appIdentifier isEqual:ids]) {
                //exit
                asm(
                    "mov X0,#0\n"
                    "mov w16,#1\n"
                    "svc #0x80"
                    );
            }
            break;
        }
    }
#endif
}


bool checkDebugger(){
#ifdef __arm64__
    //控制码
    int name[4];//放字节码-查询信息
    name[0] = CTL_KERN;//内核查看
    name[1] = KERN_PROC;//查询进程
    name[2] = KERN_PROC_PID; //通过进程id查进程
    name[3] = getpid();//拿到自己进程的id
    //查询结果
    struct kinfo_proc info;//进程查询信息结果
    size_t info_size = sizeof(info);//结构体大小
    int error = sysctl(name, sizeof(name)/sizeof(*name), &info, &info_size, 0, 0);
    assert(error == 0);//0就是没有错误
    
    //结果解析 p_flag的第12位为1就是有调试
    //p_flag 与 P_TRACED =0 就是有调试
    return ((info.kp_proc.p_flag & P_TRACED) !=0);
#endif
    return NO;
}

bool checkPath()
{
#ifdef __arm64__
    BOOL jailBroken = NO;
    NSArray * paths = @[
        @"/Applications/Cydia.app",
        @"/var/lib/cydia/",
        @"/private/var/lib/apt",
        @"/var/lib/apt",
        @"/etc/apt",
        @"/var/cache/apt",
        @"/bin/bash",
        @"/bin/sh",
        @"/usr/sbin/sshd",
        @"/usr/libexec/ssh-keysign",
        @"/etc/ssh/sshd_config",
        @"/Library/MobileSubstrate/MobileSubstrate.dylib",
    ];
    for (NSString *path in paths) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            jailBroken = YES;
            break;
        }
    }
    int ret;
    Dl_info dylib_info;
    int (*func_stat)(const char *,struct stat *) = stat;
    if ((ret = dladdr(func_stat, &dylib_info))) {
        if (strcmp(dylib_info.dli_fname,"/usr/lib/system/libsystem_kernel.dylib") != 0) {
            jailBroken = YES;
        }
    }
    
    return jailBroken;
#endif
    return NO;
}

//防hook NSFileManager的方法  使用stat系列函数检测Cydia等工具，路径同上
bool checkCydia()
{
#ifdef __arm64__
    int ret;
    Dl_info dylib_info;
    int (*func_stat)(const char *,struct stat *) = stat;
    if ((ret = dladdr(func_stat, &dylib_info))) {
        if (strcmp(dylib_info.dli_fname,"/usr/lib/system/libsystem_kernel.dylib") != 0) {
            return YES;
        }
    }
    char* paths[12] = {
        "/Applications/Cydia.app",
        "/var/lib/cydia/",
        "/private/var/lib/apt",
        "/var/lib/apt",
        "/etc/apt",
        "/var/cache/apt",
        "/bin/bash",
        "/bin/sh",
        "/usr/sbin/sshd",
        "/usr/libexec/ssh-keysign",
        "/etc/ssh/sshd_config",
        "/Library/MobileSubstrate/MobileSubstrate.dylib",
    };
    for (int i = 0; i < sizeof(paths); i++) {
        char* path = paths[i];
        struct stat stat_info;
        if (0 == stat(path, &stat_info)) {
            NSLog(@"Device is jailbroken");
            return YES;
        }
    }
#endif
    return NO;
    
}

// 检测环境改变
bool checkEnv()
{
    char *env = getenv("DYLD_INSERT_LIBRARIES");
    NSLog(@"%s", env);
    if (env) {
        return YES;
    }
    return NO;
}

#endif /* FYProtectCheck_h */
