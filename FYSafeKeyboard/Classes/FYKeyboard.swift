//
//  FYKeyboard.swift
//  hook
//
//  Created by yan on 2020/9/10.
//  Copyright © 2020 yan. All rights reserved.
//

import UIKit

class FYKeyboard: UIView {
        
    private var textField: UITextField?
    
    private var keysMap = [Int: KeyModel]()
    
    private var isUpper = false
    
    private var inputKeys = [String]()
    
    private var currentV: UIView!
    
    @IBOutlet weak var allV: UIView!
    @IBOutlet weak var numCharV: UIView!
    @IBOutlet weak var specialCharV: UIView!
    @IBOutlet weak var numV: UIView!
    @IBOutlet weak var charV: UIView!
    
    var isRandomNum = false
    
    var isRandomChar = false
            
    private override init(frame: CGRect) {
        let bottomHeight: CGFloat = UIApplication.shared.statusBarFrame.height == 20 ? 0 : 34
        let height = UIScreen.main.bounds.width * 260 / 375 + bottomHeight + 20
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        loadView()
        addObserver()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadView() {
        let FYBundle = Bundle(identifier: "org.cocoapods.FYSafeKeyboard")
        let oneView = UINib(nibName: "FYKeyboard", bundle: FYBundle).instantiate(withOwner: self, options: nil).last as! UIView
        oneView.frame = bounds
        addSubview(oneView)
    }
    
    private func initkeys() {
        if let tf = textField as? FYTextField,
            tf.safeKeyboardType == .all {
            numCharV.isHidden = true
            allV.isHidden = false
            specialCharV.isHidden = true
            numV.isHidden = true
            charV.isHidden = false
            currentV = allV
        } else {
            numCharV.isHidden = false
            allV.isHidden = true
            specialCharV.isHidden = true
            numV.isHidden = true
            charV.isHidden = true
            currentV = numCharV
        }
        isUpper = false
        let upBtn = currentV.viewWithTag(-2) as? UIButton
        upBtn?.isSelected = false
        var numKeys = FYSafeKeyboard.share.numKeys
        var charKeys = FYSafeKeyboard.share.charKeys
        let ASCIIKeys = FYSafeKeyboard.share.ASCIIKeys
        let numASCIIKeys = FYSafeKeyboard.share.numASCIIKeys
        let charASCIIKeys = FYSafeKeyboard.share.charASCIIKeys
        for i in 100..<308 {
            switch i {
            case 100..<110:
                if isRandomNum {
                    let idx = Int(arc4random() % UInt32(numKeys.count))
                    keysMap[i] = numKeys[idx]
                    numKeys.remove(at: idx)
                } else {
                    keysMap[i] = numKeys[i - 100]
                }
            case 110..<136:
                if isRandomChar {
                    let idx = Int(arc4random() % UInt32(charKeys.count))
                    keysMap[i] = charKeys[idx]
                    charKeys.remove(at: idx)
                } else {
                    keysMap[i] = charKeys[i - 110]
                }
            case 200..<235:
                keysMap[i] = ASCIIKeys[i - 200]
            case 235..<243:
                keysMap[i] = numASCIIKeys[i - 235]
            case 243..<245:
                keysMap[i] = charASCIIKeys[i - 243]
            default:
                continue
            }
            guard var lowKey = keysMap[i]?.lowercased ,
                  var upKey = keysMap[i]?.uppercased,
                  let btn = currentV.viewWithTag(i) as? UIButton else {continue}
            if let secDelegate = FYSafeKeyboard.share.delegate {
                lowKey = secDelegate.fy_safeKeyboardDecrypt(key: lowKey)
                upKey = secDelegate.fy_safeKeyboardDecrypt(key: upKey)
            }
            let highlightedColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1)
            let normalColor = UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1)
            btn.setBackgroundImage(changeToImage(by: normalColor), for: .normal)
            btn.setBackgroundImage(changeToImage(by: highlightedColor), for: .highlighted)
            btn.clipsToBounds = true
            btn.setTitle(lowKey, for: .normal)
            btn.layer.cornerRadius = 5
        }
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidBeginEditing), name: UITextField.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChange), name: UITextField.textDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidEndEditing), name: UITextField.textDidEndEditingNotification, object: nil)
    }
    
    @objc private func textFieldDidBeginEditing(notificaiton: Notification) {
        if FYSafeKeyboard.share.isDebugger {
            FYSafeKeyboard.share.resultCallback("ER0003", "该设备存在被调试风险，请谨慎使用")
        }
        textField = notificaiton.object as? UITextField
        if let tf = textField as? FYTextField {
            inputKeys = [String]()
            textField?.text = ""
            if let delegate = tf.fy_delegate {
                delegate.textFieldDidBeginEditing(tf)
            }
        }
        initkeys()
    }
    
    @objc private func textFieldDidChange(notificaiton: Notification) {
        if let tf = textField as? FYTextField {
            if let delegate = tf.fy_delegate {
                delegate.textFieldEditingChanged(tf, texts: inputKeys)
            }
        }
    }
    
    @objc private func textFieldDidEndEditing(notificaiton: Notification) {
        if let tf = textField as? FYTextField,
            let delegate = tf.fy_delegate {
            let str = inputKeys.map({
                var key = $0
                if let secDelegate = FYSafeKeyboard.share.delegate {
                    key = secDelegate.fy_safeKeyboardDecrypt(key: $0)
                }
                return key
            }).joined()
            delegate.textFieldDidEndEditing(tf, text: str)
        }
        textField = nil
    }
    
    private func getRange() -> Range<Int>? {
        if let tf = textField,
            let textRange = tf.selectedTextRange {
            let start = tf.offset(from: tf.beginningOfDocument, to: textRange.start)
            let end = tf.offset(from: tf.beginningOfDocument, to: textRange.end)
            return start..<end
        }
        return nil
    }
    
    @IBAction func keyAction(_ sender: UIButton) {
        guard let tf = textField else {return}
        if let key = keysMap[sender.tag],
            let range = getRange() {
            let char = isUpper ? key.uppercased : key.lowercased
            var value = char
            if let secDelegate = FYSafeKeyboard.share.delegate {
                value = secDelegate.fy_safeKeyboardDecrypt(key: char)
            }
            let nsRange = NSRange(location: range.startIndex, length: (range.endIndex - range.startIndex))
            if let delegate = tf.delegate {
                guard delegate.textField?(tf, shouldChangeCharactersIn: nsRange, replacementString: value) == true else {
                    return
                }
            }
            inputKeys.replaceSubrange(range, with: [char])
            if tf.isSecureTextEntry {
                tf.insertText("●")
            } else {
                tf.insertText(value)
            }
        }
    }
    
    @IBAction func upAction(_ sender: UIButton) {
        isUpper = !isUpper
        sender.isSelected = isUpper
        for i in 110..<136 {
            let btn = currentV.viewWithTag(i) as? UIButton
            var lowKey = keysMap[i]!.lowercased
            var upKey = keysMap[i]!.uppercased
            if let secDelegate = FYSafeKeyboard.share.delegate {
                lowKey = secDelegate.fy_safeKeyboardDecrypt(key: lowKey)
                upKey = secDelegate.fy_safeKeyboardDecrypt(key: upKey)
            }
            btn?.setTitle(lowKey, for: .normal)
            let key = isUpper ? upKey : lowKey
            btn?.setTitle(key, for: .normal)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if var range = getRange(),
            !inputKeys.isEmpty{
            if range.lowerBound == range.upperBound {
                range = (range.lowerBound - 1)..<range.upperBound
            }
            let nsRange = NSRange(location: range.startIndex, length: (range.endIndex - range.startIndex))
            if range.lowerBound >= 0 {
                if let tf = textField,
                    let delegate = tf.delegate {
                    guard delegate.textField?(tf, shouldChangeCharactersIn: nsRange, replacementString: "") == true else {
                        return
                    }
                }
                inputKeys.removeSubrange(range)
            }
        }
        textField?.deleteBackward()
    }
    
    @IBAction func changeToCharAction(_ sender: UIButton) {
        specialCharV.isHidden = true
        numV.isHidden = true
        charV.isHidden = false
    }
    
    @IBAction func changeToNumAction(_ sender: UIButton) {
        specialCharV.isHidden = true
        numV.isHidden = false
        charV.isHidden = true
    }
    
    @IBAction func changeToSpecialCharAction(_ sender: UIButton) {
        specialCharV.isHidden = false
        numV.isHidden = true
        charV.isHidden = true
    }
    
    
    @IBAction func commitAction(_ sender: UIButton) {
        if let tf = textField as? FYTextField,
            let delegate = tf.fy_delegate {
            let _ = delegate.textFieldShouldReturn?(tf)
        }
        textField?.resignFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- 把#ffffff颜色转为UIImage
    func changeToImage(by color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
