//
//  ViewController.swift
//  FYSafeKeyboard
//
//  Created by 757094197@qq.com on 09/11/2020.
//  Copyright (c) 2020 757094197@qq.com. All rights reserved.
//

import UIKit
import FYSafeKeyboard

class ViewController: UIViewController, FYTextFieldDelegate {
    @IBOutlet weak var lab: UILabel!
    @IBOutlet weak var tf: FYTextField!
    
    @IBOutlet weak var content: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tf.fy_delegate = self
        tf.safeKeyboardType = .all
        NotificationCenter.default.addObserver(self, selector: #selector(msgInfo(notificaion:)), name: NSNotification.Name(rawValue: "msgInfo"), object: nil)
    }
    
    @objc func msgInfo(notificaion: Notification) {
        lab.text = notificaion.userInfo?["msg"] as? String
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidEndEditing(_ textField: FYTextField, text: String) {
        print(text)
        if textField == tf {
            print("===")
            content.text = text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(string)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("commit return")
        return true
    }
    
}

