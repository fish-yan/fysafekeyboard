//
//  FYKeyboard.swift
//  hook
//
//  Created by yan on 2020/9/10.
//  Copyright © 2020 yan. All rights reserved.
//

import UIKit

class FYKeyboard: UIView {
        
    private var textField: UITextField?
    
    private var keysMap = [Int: KeyModel]()
    
    private var inputKeys = [String]()
            
    private override init(frame: CGRect) {
        let bottomHeight: CGFloat = UIApplication.shared.statusBarFrame.height == 20 ? 0 : 39
        let height = UIScreen.main.bounds.width * 2 / 5 + bottomHeight
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        loadView()
        addObserver()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadView() {
        let oneView = UINib(nibName: "FYKeyboard", bundle: nil).instantiate(withOwner: self, options: nil).last as! UIView
        oneView.frame = bounds
        addSubview(oneView)
    }
    
    private func initkeys() {
        var numKeys = FYSafeKeyboard.share.numKeys, charKeys = FYSafeKeyboard.share.charKeys
        for i in 100..<136 {
            if i < 110 {
                let idx = Int(arc4random() % UInt32(numKeys.count))
                keysMap[i] = numKeys[idx]
                numKeys.remove(at: idx)
            } else {
                let idx = Int(arc4random() % UInt32(charKeys.count))
                keysMap[i] = charKeys[idx]
                charKeys.remove(at: idx)
            }
            let btn = viewWithTag(i) as? UIButton
            var lowKey = keysMap[i]!.lowercased
            var upKey = keysMap[i]!.uppercased
            if let secDelegate = FYSafeKeyboard.share.delegate {
                lowKey = secDelegate.fy_safeKeyboardDecrypt(key: lowKey)
                upKey = secDelegate.fy_safeKeyboardDecrypt(key: upKey)
            }
            btn?.setTitle(lowKey, for: .normal)
            btn?.setTitle(upKey, for: .selected)
        }
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidBeginEditing), name: UITextField.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChange), name: UITextField.textDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidEndEditing), name: UITextField.textDidEndEditingNotification, object: nil)
    }
    
    @objc private func textFieldDidBeginEditing(notificaiton: Notification) {
        if FYSafeKeyboard.share.isDebugger {
            FYSafeKeyboard.share.resultCallback(false, "该设备存在被调试风险，请谨慎使用")
        }
        initkeys()
        textField = notificaiton.object as? UITextField
        if let tf = textField as? FYTextField,
            let delegate = tf.fy_delegate {
            delegate.textFieldDidBeginEditing(tf)
        }
    }
    
    @objc private func textFieldDidChange(notificaiton: Notification) {
        if let tf = textField as? FYTextField,
            let delegate = tf.fy_delegate {
            delegate.textFieldEditingChanged(tf, texts: inputKeys)
        }
    }
    
    @objc private func textFieldDidEndEditing(notificaiton: Notification) {
        if let tf = textField as? FYTextField,
            let delegate = tf.fy_delegate {
            let str = inputKeys.map({
                var key = $0
                if let secDelegate = FYSafeKeyboard.share.delegate {
                    key = secDelegate.fy_safeKeyboardDecrypt(key: $0)
                }
                return key
            }).joined()
            delegate.textFieldDidEndEditing(tf, text: str)
        }
        textField = nil
    }
    
    private func getRange() -> Range<Int>? {
        if let tf = textField,
            let textRange = tf.selectedTextRange {
            let start = tf.offset(from: tf.beginningOfDocument, to: textRange.start)
            let end = tf.offset(from: tf.beginningOfDocument, to: textRange.end)
            return start..<end
        }
        return nil
    }
    
    @IBAction func keyAction(_ sender: UIButton) {
        guard let tf = textField else {return}
        if let key = keysMap[sender.tag],
            let range = getRange() {
            let char = sender.isSelected ? key.uppercased : key.lowercased
            inputKeys.replaceSubrange(range, with: [char])
            if tf.isSecureTextEntry {
                tf.insertText("●")
            } else {
                var value = char
                if let secDelegate = FYSafeKeyboard.share.delegate {
                    value = secDelegate.fy_safeKeyboardDecrypt(key: char)
                }
                tf.insertText(value)
            }
        }
    }
    
    @IBAction func upAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        for i in 100..<136 {
            let btn = viewWithTag(i) as? UIButton
            btn?.isSelected = sender.isSelected
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if var range = getRange(),
            !inputKeys.isEmpty{
            if range.lowerBound == range.upperBound {
                range = (range.lowerBound - 1)..<range.upperBound
            }
            inputKeys.removeSubrange(range)
        }
        textField?.deleteBackward()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
