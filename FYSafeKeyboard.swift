//
//  FYSafeKeyboard.swift
//  hook
//
//  Created by yan on 2020/9/10.
//  Copyright © 2020 yan. All rights reserved.
//

import UIKit

protocol FYSafeKeyboardDelete: NSObjectProtocol {
    func fy_safeKeyboardEncrypt(key: String) -> String
    func fy_safeKeyboardDecrypt(key: String) -> String
}

extension FYSafeKeyboardDelete {
    func fy_safeKeyboardEncrypt(key: String) -> String {
        return FYDES3Util.encrypt(key) ?? key
    }
    func fy_safeKeyboardDecrypt(key: String) -> String {
        return FYDES3Util.decrypt(key) ?? key
    }
}

class KeyModel: NSObject {
    var uppercased = ""
    var lowercased = ""
    init(_ up: String, low: String) {
        uppercased = up
        lowercased = low
    }
}

class FYSafeKeyboard: NSObject, FYSafeKeyboardDelete {
    
    static let share = FYSafeKeyboard()
    
    var isDebugger: Bool {
        return checkDebugger()
    }
    
    weak open var delegate: FYSafeKeyboardDelete?
    
    private override init() {}
    
    private(set) var numKeys = [KeyModel]()
    
    private(set) var charKeys = [KeyModel]()
    
    private(set) var resultCallback: ((Bool, String)->Void)!
    
    /**
     - parameter debug: debug 模式下关闭防调试
     - parameter codeSign: 证书组织编号
     - parameter result: （检测结果，检测提示）
     */
    static func initKeyboard(with debug: Bool, codeSign: String, result: @escaping ((Bool, String)->Void) = {_,_ in }) {
        FYSafeKeyboard.share.resultCallback = result
        var msg = ""
        var res = true
        // 重签名检测
        checkCodesign(codeSign)
        // 越狱检测
        if checkCydia() {
            res = false
            msg = "该设备可能已越狱，请谨慎使用"
        }
        // 检测系统环境
        if checkEnv() {
            res  = false
            msg = "设备环境存在异常，请谨慎使用"
        }
        // 防调试
        if !debug {
            asm_ptrace()
        }
        // 加密所有字符
        FYSafeKeyboard.share.initKeysMap()
        result(res, msg)
    }
    
    private func initKeysMap() {
        let keys1 = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        let keys2 = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m"]
        if delegate == nil {
            delegate = self
        }
        numKeys = keys1.map({
            var key = $0
            if let deleg = delegate {
                key = deleg.fy_safeKeyboardEncrypt(key: $0)
            }
            return KeyModel(key, low: key)
        })
        charKeys = keys2.map({
            var upKey = $0.uppercased()
            var lowKey = $0.lowercased()
            if let deleg = delegate {
                upKey = deleg.fy_safeKeyboardEncrypt(key: $0.uppercased())
                lowKey = deleg.fy_safeKeyboardEncrypt(key: $0.lowercased())
            }
            return KeyModel(upKey, low: lowKey)
        })
    }
}
